﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.AppLogic
{
    class Pomiarowiec : Worker
    {
        public string monterName;
        public string monterSurname;

        public Pomiarowiec(string monterName, string monterSurname) : base(monterName, monterSurname)
        {

        }

        public override void Add(string Workertype = "Pomiarowcy")
        {
            base.Add(Workertype);
        }
        public override void Delete(string Workertype = "Pomiarowcy")
        {
            base.Delete(Workertype);
        }
    }
}
