﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.AppLogic
{
    class Monter : Worker
    {
        string monterName;
        string monterSurname;
        public Monter(string monterName, string monterSurname) : base(monterName, monterSurname)
        {

        }
        public override void Add(string Workertype = "Monterzy")
        {
            base.Add(Workertype);
        }
        public override void Delete(string Workertype = "Monterzy")
        {
            base.Delete(Workertype);
        }
    }
}
