﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Calendar.NET;
using System.Data.Sql;
using System.Data.SqlClient;

namespace AdminPanel.AppLogic
{
    public partial class CalendarView : Form
    {

        public CalendarView()
        {
            InitializeComponent();
            
            refreshEventsForCurrentMonth();
        }

        private void CalendarView_Load(object sender, EventArgs e)
        {

        }
        public void refreshEventsForCurrentMonth()
        {
            eventsToCalendar(getListofEvents("Montaz"));
            eventsToCalendar(getListofEvents("Pomiar"));
        }

        private string selectProperQuery(string type)
        {
            string month = calendar.currentCalendarDate().Month.ToString();
            string year = calendar.currentCalendarDate().Year.ToString();
            string sqlQueryMont = $"SELECT Montaż.[Data montażu], Montaż.[Godzina montażu], Ulica, [Kod pocztowy], Miasto, Uwagi FROM Klient INNER JOIN Montaż ON Klient.Id_klient = Montaż.Id_klient WHERE MONTH(Montaż.[Data montażu]) = {month} AND YEAR(Montaż.[Data montażu])={year}";
            string sqlQueryPom = $"SELECT Pomiar.[Data pomiaru], Pomiar.[Godzina Pomiaru], Ulica, [Kod pocztowy], Miasto, Uwagi FROM Klient INNER JOIN Pomiar ON Klient.Id_klient = Pomiar.Id_klient WHERE MONTH(Pomiar.[Data pomiaru]) = {month} AND YEAR(Pomiar.[Data pomiaru])={year}";
            if (type.Equals("Montaz"))
                return sqlQueryMont;
            else
                return sqlQueryPom;
        }

        private string selectProperWord(string type)
        {
            if (type.Equals("Montaz"))
                return "montażu";
            else
                return "pomiaru";
        }
        private List<ParsedEvent> getListofEvents(string type)//List<ParsedEvent> getListOfEvents()
        {
            List<ParsedEvent> loe = new List<ParsedEvent>();
            string sciezka = $@"{Directory.GetCurrentDirectory()}\Database.mdf";
            string connectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={sciezka}; Integrated Security=True";
            var calendar = new CalendarClass();
            string month = calendar.currentCalendarDate().Month.ToString();
            string year = calendar.currentCalendarDate().Year.ToString();
            var ui = new UISupport();
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var sqlQuery = selectProperQuery(type);
                var properWord = selectProperWord(type);
                using (var command = new SqlCommand(sqlQuery, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var evtMont = new ParsedEvent();
                            //  evtMont.id = Convert.ToInt32(reader["Id_Klient"]);
                            evtMont.evtType = type;
                            evtMont.eventDay = (DateTime)(reader[$"Data {properWord}"]);
                            evtMont.orderAddress = (reader["Ulica"]).ToString() + " " + (reader["Kod pocztowy"]).ToString() + " " + (reader["Miasto"]).ToString();
                            evtMont.orderDescription = (reader["Uwagi"]).ToString();
                            evtMont.eventTime = ui.toHour((reader[$"Godzina {properWord}"]).ToString());
                            
                            loe.Add(evtMont);
                        }
                    }
                }

            }
            return loe;
        }

        private void eventsToCalendar(List<ParsedEvent> evts)
        {
            foreach (var evt in evts)
            {
                //evt.eventDay = evt.eventDay.AddHours(evt.eventTime.elapseTime());
                if (evt.evtType.Equals("Montaz"))
                {
                    var measurement = new CustomEvent
                    { 
                        Date = ChangeTime(evt.eventDay, evt.eventTime.hour, evt.eventTime.min),                       
                        EventColor = Color.Green,
                        EventText = evt.eventTime.ToString() + " Montaż"
                    };
                    calendar.AddEvent(measurement);
                }
                else if (evt.evtType.Equals("Pomiar"))
                {
                    var installation = new CustomEvent
                    {
                        Date = ChangeTime(evt.eventDay, evt.eventTime.hour, evt.eventTime.min),
                        EventColor = Color.Red,
                        EventText = evt.eventTime.ToString() + " Pomiar"
                    };
                    calendar.AddEvent(installation);
                }
                else
                    throw new Exception("Invalid event type");
            }

        }
        public DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds = 0, int milliseconds = 0)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }
    }
}
