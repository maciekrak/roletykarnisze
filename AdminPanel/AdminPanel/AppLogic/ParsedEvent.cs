﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.AppLogic
{
    public sealed class Hour
    {
        public int hour { get; set; }
        public int min { get; set; }
        public Hour(int _h, int _m)
        {
            this.hour = _h;
            this.min = _m;
        }

        public override string ToString()
        {
            return this.hour.ToString() + ":" + validate();
        }

        public string validate()
        {
            return this.min == 0 ? "00" : $"{this.min}";
        }

        public double elapseTime()
        {
            return this.hour + (this.min / 60);
        }
        //public Hour StringToHour(string baseTxt)
        //{
        //    try
        //    {
        //        baseTxt.Split(':').ToArray();
        //        return new Hour
        //    }
        //}
    }

    class ParsedEvent
    {
        public DateTime eventDay { get; set; }
        public Hour eventTime { get; set; }
        public int id { get; set; }
       public string orderAddress { get; set; }
        public string evtType { get; set; }
       public string orderDescription { get; set; }

    }
}
