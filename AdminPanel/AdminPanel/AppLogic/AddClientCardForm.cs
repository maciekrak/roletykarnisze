﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminPanel.AppLogic
{
    public partial class AddClientCardForm : Form
    {
        int price1 = 0;
        int price2 = 0;

        public AddClientCardForm()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }


        //Button dodaj:
        private void button1_Click(object sender, EventArgs e)
        {
            UISupport ui = new UISupport();
            try
            { 
                var clientCard = new ClientCard(textBox1.Text, textBox2.Text);
                clientCard.AddClientCard();
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Close();
            }
        }
        //Button Anuluj:
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void textBox13_TextChanged(object sender, EventArgs e)
        {
            try
            {
                price2 = Convert.ToInt32(textBox13.Text);
                var finalPrice = Math.Abs(price2 - price1);
                label16.Text = finalPrice.ToString();              
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {
            try
            {
                price1 = Convert.ToInt32(textBox12.Text);
            }
            catch
            {
                throw new Exception("Unable to convert text to number value");
            }
            
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }
        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AddClientCardForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'databaseDataSet.Pomiarowcy' table. You can move, or remove it, as needed.
           
            // TODO: This line of code loads data into the 'mainDataSet.DataTable2' table. You can move, or remove it, as needed.

            // TODO: This line of code loads data into the 'mainDataSet.DataTable1' table. You can move, or remove it, as needed.


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }


        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged_2(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void databaseDataSetBindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void pomiarowcyBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox11_KeyPress(object sender, KeyPressEventArgs e)
        {
            blockNonNumberChars(e);
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            blockNonNumberChars(e);
        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            blockNonNumberChars(e);
        }

        private void textBox12_KeyPress(object sender, KeyPressEventArgs e)
        {
            blockNonNumberChars(e);
        }

        private void textBox13_KeyPress(object sender, KeyPressEventArgs e)
        {
            blockNonNumberChars(e);
        }

        private void blockNonNumberChars(KeyPressEventArgs e)
        {
            for (int h = 58; h <= 127; h++)
            {
                if (e.KeyChar == h)             //58 to 127 is alphabets tat will be         blocked
                {
                    e.Handled = true;
                }
            }
            for (int k = 32; k <= 47; k++)
            {
                if (e.KeyChar == k)              //32 to 47 are special characters tat will 
                {
                    e.Handled = true;
                }
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
