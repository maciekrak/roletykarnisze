﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.AppLogic
{
    class UISupport
    {
        public int GetWorkerId()
        {
            return 1;
        }
        public int GetOrderId()
        {
            return 1;
        }

        public Hour toHour(string txt)
        {
            try
            {
                var hour = txt.Split(':').ToArray();
                return (new Hour(Convert.ToInt32(hour[0]), Convert.ToInt32(hour[1])));
            }
            catch
            {
                return new Hour(0, 0);
            }
        }
    }
}
