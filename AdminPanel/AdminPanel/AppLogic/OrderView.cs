﻿using AdminPanel.PdfCreator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace AdminPanel.AppLogic
{
    public partial class OrderView : Form
    {
        public int numer_klienta;


        private List<string> loe;
        public OrderView(List<String> _loe)
        {
            this.loe = _loe;           
            InitializeComponent();
            fillLabels();
            this.numer_klienta = Convert.ToInt32(loe[0]);

        }

        private List<Product> loadProduktInfo()
        {
            var ctx = new DataContext();
            string sqlQuery = $"SELECT Typ, Model, Ilość, Wymiar, Status, Cena FROM Produkt WHERE Id_klient = {numer_klienta}";
            string connectionString = ctx.getConnectionString();
            var lop = new List<Product>();
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(sqlQuery, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.prodModel = (reader["Model"]).ToString();
                            product.prodType = (reader["Typ"]).ToString();
                            product.prodAmount = Convert.ToInt32((reader["Ilość"]));
                            product.prodPrice = Convert.ToInt32((reader["Cena"]));
                            product.prodStatus = (reader["Status"]).ToString();
                            product.prodSize = (reader["Wymiar"]).ToString();
                            lop.Add(product); 
                        }
                    }
                }
            }
            return lop;
        }

        private void richTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (richTextBox1.Text.Equals("uwagi"))
                richTextBox1.Text = "";
        }

        private void fillLabels()
        {
            label11.Text = loe[0];
            label12.Text = loe[1];
            label13.Text = loe[2];
            label14.Text = loe[3];
            label15.Text = loe[4];
            label16.Text = loe[5];
            label17.Text = loe[6].Split(' ').ToList().First();
            label18.Text = String.Join(":", loe[7].Split(':').ToList().Take(2));
            label19.Text = loe[8].Split(' ').ToList().First();
            label20.Text = String.Join(":", loe[9].Split(':').ToList().Take(2));
        }

        private void button_Click(object sender, EventArgs e)
        {
            loe.Add(richTextBox1.Text);
            var dp = new DataPrinter(loe);
            dp.dataToPdf();
        }

        private void OrderView_Load(object sender, EventArgs e)
        {

        }
    }
}
