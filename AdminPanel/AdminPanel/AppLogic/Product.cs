﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.AppLogic
{

    public class Product
    {
        public string prodModel { get; set; }
        public string prodType { get; set; }
        public string prodStatus { get; set; }
        public int prodAmount { get; set; }
        public string prodSize { get; set; }
        public int prodPrice { get; set; }

    }
}
