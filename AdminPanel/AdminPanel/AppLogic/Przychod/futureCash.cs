﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.AppLogic.Przychod
{
    class futureCash
    {
        public float approxCas;
        public float allCash;

        public futureCash()
        {
            this.allCash = setallCash();
            this.approxCas = setapproxCash();
        }
                       
        private float setallCash()
        {
            var Context = new DataContext();
            float res = 0;
            string query = $"SELECT SUM('Wartość dla klienta') AS totalCash FROM Zamowienia WHERE MONTH(Montaż.[Data montażu]) = {DateTime.Now.Month} AND YEAR(Montaż.[Data montażu])={DateTime.Now.Day}";
            try
            {
                using (var connection = new SqlConnection(Context.getConnectionString()))
                {
                    connection.Open();
                    using (var command = new SqlCommand(query, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                                res += Convert.ToSingle((reader["totalCash"]).ToString());
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message + "Nie można określić przewidywanego dochodu. Class: " + this.GetType().Name);
            }

            return res;
            }

        private float setapproxCash()
        {
            return setallCash() * getRemainingDays() / getPastDays();
        }

        private float getPastDays()
        {
            return DateTime.Now.Day;
        }

        private float getRemainingDays()
        {
            if (DateTime.MaxValue.Day - DateTime.Now.Day == 0)
                return 1;
            else
                return (DateTime.MaxValue.Day - DateTime.Now.Day);
        }
    }
}
