﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.AppLogic
{
    interface IWorker
    {
        void Add(string Workertype);
        void Delete(string Workertype);
        void Edit();
        int GetId(string Workertype, string Workername, string WorkerSurname);
    }
}
