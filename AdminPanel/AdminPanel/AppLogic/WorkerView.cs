﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminPanel.AppLogic
{
    public partial class WorkerView : Form
    {
        public string imie;
        public string nazwisko;
        public string rola;
        private readonly MainUI ui;
        public WorkerView(string _imie, string _nazwisko, string _rola, MainUI _ui)
        {
            this.imie = _imie;
            this.nazwisko = _nazwisko;
            this.rola = _rola;
            this.ui = _ui;
            InitializeComponent();
            updateLabels();
        }

        private void WorkerView_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void updateLabels()
        {
            label1.Text = (this.imie == null ? "popsulosie" : this.imie);
            label2.Text = (this.nazwisko == null ? "popsulosie" : this.nazwisko);
            label3.Text = (this.rola == null ? "" : this.rola);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
                deleteRow();         
        }
        public void deleteRow()
        {
            if (this.rola.Equals("Pomiarowiec"))
            {
                var pomiarowiec = new Pomiarowiec(this.imie, this.nazwisko);
                pomiarowiec.Delete();
                ui.ticker();
                ui.Refresh();
                this.Close();
            }
            else
            {
                var monter = new Monter(this.imie, this.nazwisko);
                monter.Delete();
                ui.ticker();
                ui.Refresh();
                this.Close();
            }
        }
}
}
