﻿namespace AdminPanel.AppLogic
{
    partial class CalendarView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calendar = new Calendar.NET.CalendarClass();
            this.SuspendLayout();
            // 
            // calendar2
            // 
            this.calendar.AllowEditingEvents = true;
            this.calendar.CalendarDate = new System.DateTime(2017, 8, 18, 15, 46, 45, 736);
            this.calendar.CalendarView = Calendar.NET.CalendarViews.Month;
            this.calendar.DateHeaderFont = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.calendar.DayOfWeekFont = new System.Drawing.Font("Arial", 10F);
            this.calendar.DaysFont = new System.Drawing.Font("Arial", 10F);
            this.calendar.DayViewTimeFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.calendar.DimDisabledEvents = true;
            this.calendar.HighlightCurrentDay = true;
            this.calendar.LoadPresetHolidays = true;
            this.calendar.Location = new System.Drawing.Point(12, 12);
            this.calendar.Name = "calendar2";
            this.calendar.ShowArrowControls = true;
            this.calendar.ShowDashedBorderOnDisabledEvents = true;
            this.calendar.ShowDateInHeader = true;
            this.calendar.ShowDisabledEvents = false;
            this.calendar.ShowEventTooltips = true;
            this.calendar.ShowTodayButton = true;
            this.calendar.Size = new System.Drawing.Size(1014, 580);
            this.calendar.TabIndex = 0;
            this.calendar.TodayFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.calendar.Load += new System.EventHandler(this.CalendarView_Load);
            // 
            // CalendarView
            // 
            this.ClientSize = new System.Drawing.Size(1029, 598);
            this.Controls.Add(this.calendar);
            this.Name = "CalendarView";
            this.Load += new System.EventHandler(this.CalendarView_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Calendar.NET.CalendarClass calendar;
    }
}