﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel
{
    public class ClientCard
    {
        public int? cardNumber { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Adress { get; set; }
        public string District { get; set; }
        public string Town { get; set; }
        public string postCode { get; set; }
        public string phoneNumber { get; set; }
        public string addInformation { get; set; }
        public DateTime measureDate { get; set; }
        public DateTime installationDate { get; set; }
        public int installationTime { get; set; }
        public int measureTime { get; set; }
        public int installationWorker { get; set; }
        public int measureWorker { get; set; }
        public int priceForClient { get; set; }
        public int priceFromPricelist { get; set; }
        public string payOffType { get; set; }
        public string advancePayment { get; set; }
        public string products;

        public ClientCard(string name, string surname)
        {
            this.Name = name;
            this.Surname = surname;
        }

        public void AddClientCard()
        {
            var ctx = new DataContext();
            string connectionString = ctx.getConnectionString();
            string query = $"INSERT INTO Klient(Imie, Nazwisko, Adres, Dzielnica, Miasto, 'Kod Pocztowy', 'Numer Telefonu', Uwagi, 'Data pomiaru', 'Czas pomiaru', Pomiarowiec, 'Data montażu', 'Czas montażu', " +
                $"Monter, 'Zamówione produkty', Zaliczka, 'Wartość cennikowa', 'Wartość dla klienta', Rozliczenie) VALUES (@Name, @Surname, @Adress, @District, @Town, @postCode, @phoneNumber, @measureDate, @installationDate, " +
                $"@addInformation, @installationTime, @measureTime, @installationWorker, @measureWorker, @priceForClient, @priceFromPriceList, @payOffType, @advancePayment)";
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Imie", Name);
                    command.Parameters.AddWithValue("@Nazwisko", Surname);
                    command.Parameters.AddWithValue("@Ulica", Adress);
                    command.Parameters.AddWithValue("@Dzielnica", District);
                    command.Parameters.AddWithValue("@Miasto", Town);
                    command.Parameters.AddWithValue("@Kod Pocztowy", postCode);
                    command.Parameters.AddWithValue("@Numer Telefonu", phoneNumber);
                    command.Parameters.AddWithValue("@Uwagi", addInformation);
                    command.Parameters.AddWithValue("@Data Pomiaru", measureDate);
                    command.Parameters.AddWithValue("@Czas Pomiaru", measureTime);
                    command.Parameters.AddWithValue("@Pomiarowiec", measureWorker);
                    command.Parameters.AddWithValue("@Data Montażu", installationDate);
                    command.Parameters.AddWithValue("@Czas Montażu", installationTime);
                    command.Parameters.AddWithValue("@Monter", installationWorker);
                    command.Parameters.AddWithValue("@monterSurname", Surname);
                    command.Parameters.AddWithValue("@Zamówione Produkty", products);
                    command.Parameters.AddWithValue("@Zaliczka", advancePayment);
                    command.Parameters.AddWithValue("@Wartość cennikowa", priceFromPricelist);
                    command.Parameters.AddWithValue("@Wartość dla klienta", priceForClient);
                    command.Parameters.AddWithValue("@Rozliczenie", payOffType);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
