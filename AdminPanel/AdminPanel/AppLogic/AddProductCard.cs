﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace AdminPanel.AppLogic
{
    public partial class AddProductCard : Form
    {

        public string Status;
        public string ProductType;       
        private int Quantity;
        private string Model;
        private string Dimensions;
        private int Price;
        private int ClientId;

        public AddProductCard()
        {
            InitializeComponent();
            fillComboBox();
        }

        public void ProductCard()
        {
            string sciezka = $@"{Directory.GetCurrentDirectory()}\Database.mdf";
            string connectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={sciezka}; Integrated Security=True";
            string query = $"INSERT INTO Produkt(Typ, Model, Ilość, Wymiar, Id_klient, Cena, Status) VALUES (@ProductType, @Model, @Quantity, @Dimensions, @ClientId, @Price, @Status)";
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Typ", ProductType);
                    command.Parameters.AddWithValue("@Model", Model);
                    command.Parameters.AddWithValue("@Ilość", Quantity);
                    command.Parameters.AddWithValue("@Wymiar", Dimensions);
                    command.Parameters.AddWithValue("@Id_klient", ClientId);
                    command.Parameters.AddWithValue("@Cena", Price);
                    command.Parameters.AddWithValue("@Status", Status);
                    command.ExecuteNonQuery();
                }  
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProductType = comboBox1.Text.ToString();
        }
        private void fillComboBox()
        {
            comboBox1.DataSource = Enum.GetValues(typeof(ProductTypes));
            comboBox2.DataSource = Enum.GetValues(typeof(ProductStatus));
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Status = comboBox2.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProductCard();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddProductCard_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'databaseDataSet.Klient1' table. You can move, or remove it, as needed.
            this.klient1TableAdapter.Fill(this.databaseDataSet.Klient1);

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClientId = Convert.ToInt32(comboBox3.Text.Split(' ').First());
            }
            catch
            {
                ClientId = 0;
            }
        }

        public enum ProductTypes
        {
            Roleta,
            Karnisz,
            Plisa,
            Moskitiera,
            Żaluzja
        }
        public enum ProductStatus
        {
            Dostepny,
            Przygotowany_do_montazu,
            Zamonotowany
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            Dimensions = textBox3.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Quantity = Convert.ToInt32(textBox2.Text);
        }
        private void blockNonNumberChars(KeyPressEventArgs e)
        {
            for (int h = 58; h <= 127; h++)
            {
                if (e.KeyChar == h)             //58 to 127 is alphabets tat will be         blocked
                {
                    e.Handled = true;
                }
            }
            for (int k = 32; k <= 47; k++)
            {
                if (e.KeyChar == k)              //32 to 47 are special characters tat will 
                {
                    e.Handled = true;
                }
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            blockNonNumberChars(e);
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            blockNonNumberChars(e);
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            Price = Convert.ToInt32(textBox4.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Model = textBox1.Text;
        }
    }
}
