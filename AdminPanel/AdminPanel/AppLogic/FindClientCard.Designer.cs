﻿namespace AdminPanel.AppLogic
{
    partial class FindClientCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.databaseDataSet = new AdminPanel.DatabaseDataSet();
            this.zamowieniaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.zamowieniaTableAdapter = new AdminPanel.DatabaseDataSetTableAdapters.ZamowieniaTableAdapter();
            this.idklientDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwiskoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ulicaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miastoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numerTelefonuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wartośćDlaKlientaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataMontażuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.godzinaMontażuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataPomiaruDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.godzinaPomiaruDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zamowieniaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Id_klient",
            "Nazwisko",
            "Data montażu",
            "Numer telefonu"});
            this.comboBox1.Location = new System.Drawing.Point(480, 34);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(505, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wyszukaj po:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(480, 78);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(483, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Podaj słowo kluczowe:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(501, 104);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Szukaj";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idklientDataGridViewTextBoxColumn,
            this.nazwiskoDataGridViewTextBoxColumn,
            this.ulicaDataGridViewTextBoxColumn,
            this.miastoDataGridViewTextBoxColumn,
            this.numerTelefonuDataGridViewTextBoxColumn,
            this.wartośćDlaKlientaDataGridViewTextBoxColumn,
            this.dataMontażuDataGridViewTextBoxColumn,
            this.godzinaMontażuDataGridViewTextBoxColumn,
            this.dataPomiaruDataGridViewTextBoxColumn,
            this.godzinaPomiaruDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.zamowieniaBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(28, 136);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1048, 319);
            this.dataGridView1.TabIndex = 4;
            // 
            // databaseDataSet
            // 
            this.databaseDataSet.DataSetName = "DatabaseDataSet";
            this.databaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // zamowieniaBindingSource
            // 
            this.zamowieniaBindingSource.DataMember = "Zamowienia";
            this.zamowieniaBindingSource.DataSource = this.databaseDataSet;
            // 
            // zamowieniaTableAdapter
            // 
            this.zamowieniaTableAdapter.ClearBeforeFill = true;
            // 
            // idklientDataGridViewTextBoxColumn
            // 
            this.idklientDataGridViewTextBoxColumn.DataPropertyName = "Id_klient";
            this.idklientDataGridViewTextBoxColumn.HeaderText = "Id_klient";
            this.idklientDataGridViewTextBoxColumn.Name = "idklientDataGridViewTextBoxColumn";
            this.idklientDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nazwiskoDataGridViewTextBoxColumn
            // 
            this.nazwiskoDataGridViewTextBoxColumn.DataPropertyName = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn.HeaderText = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn.Name = "nazwiskoDataGridViewTextBoxColumn";
            // 
            // ulicaDataGridViewTextBoxColumn
            // 
            this.ulicaDataGridViewTextBoxColumn.DataPropertyName = "Ulica";
            this.ulicaDataGridViewTextBoxColumn.HeaderText = "Ulica";
            this.ulicaDataGridViewTextBoxColumn.Name = "ulicaDataGridViewTextBoxColumn";
            // 
            // miastoDataGridViewTextBoxColumn
            // 
            this.miastoDataGridViewTextBoxColumn.DataPropertyName = "Miasto";
            this.miastoDataGridViewTextBoxColumn.HeaderText = "Miasto";
            this.miastoDataGridViewTextBoxColumn.Name = "miastoDataGridViewTextBoxColumn";
            // 
            // numerTelefonuDataGridViewTextBoxColumn
            // 
            this.numerTelefonuDataGridViewTextBoxColumn.DataPropertyName = "Numer telefonu";
            this.numerTelefonuDataGridViewTextBoxColumn.HeaderText = "Numer telefonu";
            this.numerTelefonuDataGridViewTextBoxColumn.Name = "numerTelefonuDataGridViewTextBoxColumn";
            // 
            // wartośćDlaKlientaDataGridViewTextBoxColumn
            // 
            this.wartośćDlaKlientaDataGridViewTextBoxColumn.DataPropertyName = "Wartość dla klienta";
            this.wartośćDlaKlientaDataGridViewTextBoxColumn.HeaderText = "Wartość dla klienta";
            this.wartośćDlaKlientaDataGridViewTextBoxColumn.Name = "wartośćDlaKlientaDataGridViewTextBoxColumn";
            // 
            // dataMontażuDataGridViewTextBoxColumn
            // 
            this.dataMontażuDataGridViewTextBoxColumn.DataPropertyName = "Data montażu";
            this.dataMontażuDataGridViewTextBoxColumn.HeaderText = "Data montażu";
            this.dataMontażuDataGridViewTextBoxColumn.Name = "dataMontażuDataGridViewTextBoxColumn";
            // 
            // godzinaMontażuDataGridViewTextBoxColumn
            // 
            this.godzinaMontażuDataGridViewTextBoxColumn.DataPropertyName = "Godzina montażu";
            this.godzinaMontażuDataGridViewTextBoxColumn.HeaderText = "Godzina montażu";
            this.godzinaMontażuDataGridViewTextBoxColumn.Name = "godzinaMontażuDataGridViewTextBoxColumn";
            // 
            // dataPomiaruDataGridViewTextBoxColumn
            // 
            this.dataPomiaruDataGridViewTextBoxColumn.DataPropertyName = "Data pomiaru";
            this.dataPomiaruDataGridViewTextBoxColumn.HeaderText = "Data pomiaru";
            this.dataPomiaruDataGridViewTextBoxColumn.Name = "dataPomiaruDataGridViewTextBoxColumn";
            // 
            // godzinaPomiaruDataGridViewTextBoxColumn
            // 
            this.godzinaPomiaruDataGridViewTextBoxColumn.DataPropertyName = "Godzina pomiaru";
            this.godzinaPomiaruDataGridViewTextBoxColumn.HeaderText = "Godzina pomiaru";
            this.godzinaPomiaruDataGridViewTextBoxColumn.Name = "godzinaPomiaruDataGridViewTextBoxColumn";
            // 
            // FindClientCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 467);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Name = "FindClientCard";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FindClientCard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zamowieniaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private DatabaseDataSet databaseDataSet;
        private System.Windows.Forms.BindingSource zamowieniaBindingSource;
        private DatabaseDataSetTableAdapters.ZamowieniaTableAdapter zamowieniaTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idklientDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwiskoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ulicaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn miastoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numerTelefonuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wartośćDlaKlientaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataMontażuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn godzinaMontażuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataPomiaruDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn godzinaPomiaruDataGridViewTextBoxColumn;
    }
}