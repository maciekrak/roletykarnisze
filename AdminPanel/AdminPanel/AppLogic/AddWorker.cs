﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminPanel.AppLogic
{
    public partial class AddWorker : Form
    {
        private static string workerName, workerSurname;
        private static Monter mnt;
        private static Pomiarowiec pmr;
        private readonly MainUI ui;
        public AddWorker(MainUI _ui)
        {
            this.ui = _ui;
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            workerName = textBox1.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ManageSelectedIndex();            
            }
            finally
            {
                this.Close();
            }
            //ADD Monter to database
        }
        private void ManageSelectedIndex()
        {
            if (comboBox1.SelectedIndex == 1)
            {
                mnt = new Monter(workerName, workerSurname);
                mnt.Add();
            //    ui.ticker();
                ui.Refresh();
            }
            else
            {
                pmr = new Pomiarowiec(workerName, workerSurname);
                pmr.Add();
             //   ui.ticker();
                ui.Refresh();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddWorker_Load(object sender, EventArgs e)
        {
            FillComboBox();
        }
        private enum workerType{
            Pomiarowiec = 0,
            Monter = 1
        }
        private void FillComboBox()
        {
            comboBox1.DataSource = Enum.GetValues(typeof(workerType));
        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {
            workerSurname = textBox2.Text;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}
