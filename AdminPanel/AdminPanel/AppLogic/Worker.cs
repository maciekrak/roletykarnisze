﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;


namespace AdminPanel.AppLogic
{
    public abstract class Worker : IWorker
    {
        int monterId;
        string monterName;
        string monterSurname;

        public Worker(string monterName, string monterSurname)
        {
            this.monterName = monterName;
            this.monterSurname = monterSurname;
        }

        public virtual void Add(string Workertype)
        {
            var ctx = new DataContext();
            string query = $"INSERT INTO {Workertype}(Imie, Nazwisko) VALUES (@monterName, @monterSurname)";
            using (var connection = new SqlConnection(ctx.getConnectionString()))
            {
                connection.Open();
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@monterName", monterName);
                    command.Parameters.AddWithValue("@monterSurname", monterSurname);
                    command.ExecuteNonQuery();
                }
            }
        }

        public virtual void Delete(string Workertype)
        {
            var ctx = new DataContext();
            string query = " ";
            int id = GetId(Workertype, this.monterName, this.monterSurname);
            if (Workertype.Equals("Pomiarowcy"))            
                query = $"DELETE FROM {Workertype} WHERE Id_pomiarowiec = {id}";
            else
                query = $"DELETE FROM {Workertype} WHERE Id_monter = {id}";

            try
            {
                using (var connection = new SqlConnection(ctx.getConnectionString()))
                {
                    connection.Open();
                    using (var command = new SqlCommand(query, connection))
                    {
                        try
                        {
                            command.ExecuteNonQuery();
                            MessageBox.Show("delete successful");
                        }
                        catch
                        {
                            MessageBox.Show("\nMonter/Pomiarowiec jest przypisany do zamówienia. Nie można usunąć.");
                        }

                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Edit()
        {
            throw new NotImplementedException();
        }

        public int GetId(string Workertype, string Workername, string WorkerSurname)
        {
            string query;
            int val = 0;
            var ctx = new DataContext();
            if (Workertype.Equals("Pomiarowcy"))
                query = $"SELECT Id_pomiarowiec FROM {Workertype} WHERE Imie = '{Workername}' and Nazwisko = '{WorkerSurname}'";
            else
                query = $"SELECT Id_monter FROM {Workertype} WHERE Imie = '{Workername}' and Nazwisko = '{WorkerSurname}'";
            try
            {
                using (var connection = new SqlConnection((ctx.getConnectionString())))
                {
                    connection.Open();
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.ExecuteNonQuery();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            reader.Read();
                            val = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return val;
        }
    }
}
