﻿namespace AdminPanel
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sAsdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.godzinaPomiaruDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imieDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwiskoDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ulicaDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dzielnicaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miastoDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numerTelefonuDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pomiaryTodayBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.databaseDataSet1 = new AdminPanel.DatabaseDataSet();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.godzinaMontażuDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imieDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwiskoDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ulicaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dzielnicaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miastoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numerTelefonuDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montażeTodayBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.databaseDataSet = new AdminPanel.DatabaseDataSet();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Zamowienia = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwiskoDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ulicaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miastoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numerTelefonuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wartośćDlaKlientaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataMontażuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.godzinaMontażuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataPomiaruDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.godzinaPomiaruDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zamowieniaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Pracownicy = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imieDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwiskoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pomiarowcyViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imieDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwiskoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monterzyViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.monterzyViewTableAdapter = new AdminPanel.DatabaseDataSetTableAdapters.MonterzyViewTableAdapter();
            this.pomiarowcyViewTableAdapter = new AdminPanel.DatabaseDataSetTableAdapters.PomiarowcyViewTableAdapter();
            this.zamowieniaTableAdapter = new AdminPanel.DatabaseDataSetTableAdapters.ZamowieniaTableAdapter();
            this.montażeTodayTableAdapter = new AdminPanel.DatabaseDataSetTableAdapters.MontażeTodayTableAdapter();
            this.pomiaryTodayTableAdapter = new AdminPanel.DatabaseDataSetTableAdapters.PomiaryTodayTableAdapter();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pomiaryTodayBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.montażeTodayBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet)).BeginInit();
            this.Zamowienia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zamowieniaBindingSource)).BeginInit();
            this.Pracownicy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pomiarowcyViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monterzyViewBindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.sAsdToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(106, 48);
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.dodajToolStripMenuItem.Text = "Dodaj";
            // 
            // sAsdToolStripMenuItem
            // 
            this.sAsdToolStripMenuItem.Name = "sAsdToolStripMenuItem";
            this.sAsdToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.sAsdToolStripMenuItem.Text = "SAsd";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(26, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 59);
            this.button1.TabIndex = 6;
            this.button1.Text = "Dodaj pracownika";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(467, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 59);
            this.button2.TabIndex = 6;
            this.button2.Text = "Dodaj zamówienie";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(175, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(116, 59);
            this.button3.TabIndex = 6;
            this.button3.Text = "Dodaj produkt";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(319, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(116, 59);
            this.button4.TabIndex = 6;
            this.button4.Text = "Pokaż kalendarz";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView6);
            this.tabPage1.Controls.Add(this.dataGridView5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1222, 504);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Dzień pracy";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView6
            // 
            this.dataGridView6.AutoGenerateColumns = false;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.godzinaPomiaruDataGridViewTextBoxColumn1,
            this.imieDataGridViewTextBoxColumn3,
            this.nazwiskoDataGridViewTextBoxColumn4,
            this.ulicaDataGridViewTextBoxColumn2,
            this.dzielnicaDataGridViewTextBoxColumn1,
            this.miastoDataGridViewTextBoxColumn2,
            this.numerTelefonuDataGridViewTextBoxColumn2});
            this.dataGridView6.DataSource = this.pomiaryTodayBindingSource;
            this.dataGridView6.Location = new System.Drawing.Point(631, 30);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(575, 469);
            this.dataGridView6.TabIndex = 6;
            // 
            // godzinaPomiaruDataGridViewTextBoxColumn1
            // 
            this.godzinaPomiaruDataGridViewTextBoxColumn1.DataPropertyName = "Godzina pomiaru";
            this.godzinaPomiaruDataGridViewTextBoxColumn1.HeaderText = "Godzina pomiaru";
            this.godzinaPomiaruDataGridViewTextBoxColumn1.Name = "godzinaPomiaruDataGridViewTextBoxColumn1";
            this.godzinaPomiaruDataGridViewTextBoxColumn1.Width = 60;
            // 
            // imieDataGridViewTextBoxColumn3
            // 
            this.imieDataGridViewTextBoxColumn3.DataPropertyName = "Imie";
            this.imieDataGridViewTextBoxColumn3.HeaderText = "Imie pomiarowca";
            this.imieDataGridViewTextBoxColumn3.Name = "imieDataGridViewTextBoxColumn3";
            this.imieDataGridViewTextBoxColumn3.Width = 70;
            // 
            // nazwiskoDataGridViewTextBoxColumn4
            // 
            this.nazwiskoDataGridViewTextBoxColumn4.DataPropertyName = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn4.HeaderText = "Nazwisko pomiarowca";
            this.nazwiskoDataGridViewTextBoxColumn4.Name = "nazwiskoDataGridViewTextBoxColumn4";
            this.nazwiskoDataGridViewTextBoxColumn4.Width = 80;
            // 
            // ulicaDataGridViewTextBoxColumn2
            // 
            this.ulicaDataGridViewTextBoxColumn2.DataPropertyName = "Ulica";
            this.ulicaDataGridViewTextBoxColumn2.HeaderText = "Ulica";
            this.ulicaDataGridViewTextBoxColumn2.Name = "ulicaDataGridViewTextBoxColumn2";
            this.ulicaDataGridViewTextBoxColumn2.Width = 90;
            // 
            // dzielnicaDataGridViewTextBoxColumn1
            // 
            this.dzielnicaDataGridViewTextBoxColumn1.DataPropertyName = "Dzielnica";
            this.dzielnicaDataGridViewTextBoxColumn1.HeaderText = "Dzielnica";
            this.dzielnicaDataGridViewTextBoxColumn1.Name = "dzielnicaDataGridViewTextBoxColumn1";
            this.dzielnicaDataGridViewTextBoxColumn1.Width = 70;
            // 
            // miastoDataGridViewTextBoxColumn2
            // 
            this.miastoDataGridViewTextBoxColumn2.DataPropertyName = "Miasto";
            this.miastoDataGridViewTextBoxColumn2.HeaderText = "Miasto";
            this.miastoDataGridViewTextBoxColumn2.Name = "miastoDataGridViewTextBoxColumn2";
            this.miastoDataGridViewTextBoxColumn2.Width = 80;
            // 
            // numerTelefonuDataGridViewTextBoxColumn2
            // 
            this.numerTelefonuDataGridViewTextBoxColumn2.DataPropertyName = "Numer telefonu";
            this.numerTelefonuDataGridViewTextBoxColumn2.HeaderText = "Numer telefonu";
            this.numerTelefonuDataGridViewTextBoxColumn2.Name = "numerTelefonuDataGridViewTextBoxColumn2";
            this.numerTelefonuDataGridViewTextBoxColumn2.Width = 80;
            // 
            // pomiaryTodayBindingSource
            // 
            this.pomiaryTodayBindingSource.DataMember = "PomiaryToday";
            this.pomiaryTodayBindingSource.DataSource = this.databaseDataSet1;
            // 
            // databaseDataSet1
            // 
            this.databaseDataSet1.DataSetName = "DatabaseDataSet";
            this.databaseDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView5
            // 
            this.dataGridView5.AutoGenerateColumns = false;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.godzinaMontażuDataGridViewTextBoxColumn1,
            this.imieDataGridViewTextBoxColumn2,
            this.nazwiskoDataGridViewTextBoxColumn3,
            this.ulicaDataGridViewTextBoxColumn1,
            this.dzielnicaDataGridViewTextBoxColumn,
            this.miastoDataGridViewTextBoxColumn1,
            this.numerTelefonuDataGridViewTextBoxColumn1});
            this.dataGridView5.DataSource = this.montażeTodayBindingSource;
            this.dataGridView5.Location = new System.Drawing.Point(19, 30);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(575, 469);
            this.dataGridView5.TabIndex = 5;
            this.dataGridView5.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView5_CellContentClick);
            // 
            // godzinaMontażuDataGridViewTextBoxColumn1
            // 
            this.godzinaMontażuDataGridViewTextBoxColumn1.DataPropertyName = "Godzina montażu";
            this.godzinaMontażuDataGridViewTextBoxColumn1.HeaderText = "Godzina montażu";
            this.godzinaMontażuDataGridViewTextBoxColumn1.Name = "godzinaMontażuDataGridViewTextBoxColumn1";
            this.godzinaMontażuDataGridViewTextBoxColumn1.Width = 60;
            // 
            // imieDataGridViewTextBoxColumn2
            // 
            this.imieDataGridViewTextBoxColumn2.DataPropertyName = "Imie";
            this.imieDataGridViewTextBoxColumn2.HeaderText = "Imie montera";
            this.imieDataGridViewTextBoxColumn2.Name = "imieDataGridViewTextBoxColumn2";
            this.imieDataGridViewTextBoxColumn2.Width = 70;
            // 
            // nazwiskoDataGridViewTextBoxColumn3
            // 
            this.nazwiskoDataGridViewTextBoxColumn3.DataPropertyName = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn3.HeaderText = "Nazwisko montera";
            this.nazwiskoDataGridViewTextBoxColumn3.Name = "nazwiskoDataGridViewTextBoxColumn3";
            this.nazwiskoDataGridViewTextBoxColumn3.Width = 80;
            // 
            // ulicaDataGridViewTextBoxColumn1
            // 
            this.ulicaDataGridViewTextBoxColumn1.DataPropertyName = "Ulica";
            this.ulicaDataGridViewTextBoxColumn1.HeaderText = "Ulica";
            this.ulicaDataGridViewTextBoxColumn1.Name = "ulicaDataGridViewTextBoxColumn1";
            this.ulicaDataGridViewTextBoxColumn1.Width = 90;
            // 
            // dzielnicaDataGridViewTextBoxColumn
            // 
            this.dzielnicaDataGridViewTextBoxColumn.DataPropertyName = "Dzielnica";
            this.dzielnicaDataGridViewTextBoxColumn.HeaderText = "Dzielnica";
            this.dzielnicaDataGridViewTextBoxColumn.Name = "dzielnicaDataGridViewTextBoxColumn";
            this.dzielnicaDataGridViewTextBoxColumn.Width = 70;
            // 
            // miastoDataGridViewTextBoxColumn1
            // 
            this.miastoDataGridViewTextBoxColumn1.DataPropertyName = "Miasto";
            this.miastoDataGridViewTextBoxColumn1.HeaderText = "Miasto";
            this.miastoDataGridViewTextBoxColumn1.Name = "miastoDataGridViewTextBoxColumn1";
            this.miastoDataGridViewTextBoxColumn1.Width = 80;
            // 
            // numerTelefonuDataGridViewTextBoxColumn1
            // 
            this.numerTelefonuDataGridViewTextBoxColumn1.DataPropertyName = "Numer telefonu";
            this.numerTelefonuDataGridViewTextBoxColumn1.HeaderText = "Numer telefonu";
            this.numerTelefonuDataGridViewTextBoxColumn1.Name = "numerTelefonuDataGridViewTextBoxColumn1";
            this.numerTelefonuDataGridViewTextBoxColumn1.Width = 80;
            // 
            // montażeTodayBindingSource
            // 
            this.montażeTodayBindingSource.DataMember = "MontażeToday";
            this.montażeTodayBindingSource.DataSource = this.databaseDataSet;
            // 
            // databaseDataSet
            // 
            this.databaseDataSet.DataSetName = "DatabaseDataSet";
            this.databaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(867, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Dzisiejsze montaże";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(260, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Dzisiejsze pomiary";
            // 
            // Zamowienia
            // 
            this.Zamowienia.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.Zamowienia.Controls.Add(this.dataGridView4);
            this.Zamowienia.Location = new System.Drawing.Point(4, 22);
            this.Zamowienia.Name = "Zamowienia";
            this.Zamowienia.Padding = new System.Windows.Forms.Padding(3);
            this.Zamowienia.Size = new System.Drawing.Size(1222, 504);
            this.Zamowienia.TabIndex = 1;
            this.Zamowienia.Text = "Zamowienia";
            this.Zamowienia.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoGenerateColumns = false;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.nazwiskoDataGridViewTextBoxColumn2,
            this.ulicaDataGridViewTextBoxColumn,
            this.miastoDataGridViewTextBoxColumn,
            this.numerTelefonuDataGridViewTextBoxColumn,
            this.wartośćDlaKlientaDataGridViewTextBoxColumn,
            this.dataMontażuDataGridViewTextBoxColumn,
            this.godzinaMontażuDataGridViewTextBoxColumn,
            this.dataPomiaruDataGridViewTextBoxColumn,
            this.godzinaPomiaruDataGridViewTextBoxColumn});
            this.dataGridView4.DataSource = this.zamowieniaBindingSource;
            this.dataGridView4.Location = new System.Drawing.Point(41, 8);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(1141, 491);
            this.dataGridView4.TabIndex = 0;
            this.dataGridView4.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellContentDoubleClick);
            this.dataGridView4.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id_klient";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // nazwiskoDataGridViewTextBoxColumn2
            // 
            this.nazwiskoDataGridViewTextBoxColumn2.DataPropertyName = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn2.HeaderText = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn2.Name = "nazwiskoDataGridViewTextBoxColumn2";
            // 
            // ulicaDataGridViewTextBoxColumn
            // 
            this.ulicaDataGridViewTextBoxColumn.DataPropertyName = "Ulica";
            this.ulicaDataGridViewTextBoxColumn.HeaderText = "Ulica";
            this.ulicaDataGridViewTextBoxColumn.Name = "ulicaDataGridViewTextBoxColumn";
            this.ulicaDataGridViewTextBoxColumn.Width = 125;
            // 
            // miastoDataGridViewTextBoxColumn
            // 
            this.miastoDataGridViewTextBoxColumn.DataPropertyName = "Miasto";
            this.miastoDataGridViewTextBoxColumn.HeaderText = "Miasto";
            this.miastoDataGridViewTextBoxColumn.Name = "miastoDataGridViewTextBoxColumn";
            // 
            // numerTelefonuDataGridViewTextBoxColumn
            // 
            this.numerTelefonuDataGridViewTextBoxColumn.DataPropertyName = "Numer telefonu";
            this.numerTelefonuDataGridViewTextBoxColumn.HeaderText = "Numer telefonu";
            this.numerTelefonuDataGridViewTextBoxColumn.Name = "numerTelefonuDataGridViewTextBoxColumn";
            // 
            // wartośćDlaKlientaDataGridViewTextBoxColumn
            // 
            this.wartośćDlaKlientaDataGridViewTextBoxColumn.DataPropertyName = "Wartość dla klienta";
            this.wartośćDlaKlientaDataGridViewTextBoxColumn.HeaderText = "Wartość dla klienta";
            this.wartośćDlaKlientaDataGridViewTextBoxColumn.Name = "wartośćDlaKlientaDataGridViewTextBoxColumn";
            this.wartośćDlaKlientaDataGridViewTextBoxColumn.Width = 120;
            // 
            // dataMontażuDataGridViewTextBoxColumn
            // 
            this.dataMontażuDataGridViewTextBoxColumn.DataPropertyName = "Data montażu";
            this.dataMontażuDataGridViewTextBoxColumn.HeaderText = "Data montażu";
            this.dataMontażuDataGridViewTextBoxColumn.Name = "dataMontażuDataGridViewTextBoxColumn";
            this.dataMontażuDataGridViewTextBoxColumn.Width = 125;
            // 
            // godzinaMontażuDataGridViewTextBoxColumn
            // 
            this.godzinaMontażuDataGridViewTextBoxColumn.DataPropertyName = "Godzina montażu";
            this.godzinaMontażuDataGridViewTextBoxColumn.HeaderText = "Godzina montażu";
            this.godzinaMontażuDataGridViewTextBoxColumn.Name = "godzinaMontażuDataGridViewTextBoxColumn";
            this.godzinaMontażuDataGridViewTextBoxColumn.Width = 125;
            // 
            // dataPomiaruDataGridViewTextBoxColumn
            // 
            this.dataPomiaruDataGridViewTextBoxColumn.DataPropertyName = "Data pomiaru";
            this.dataPomiaruDataGridViewTextBoxColumn.HeaderText = "Data pomiaru";
            this.dataPomiaruDataGridViewTextBoxColumn.Name = "dataPomiaruDataGridViewTextBoxColumn";
            this.dataPomiaruDataGridViewTextBoxColumn.Width = 125;
            // 
            // godzinaPomiaruDataGridViewTextBoxColumn
            // 
            this.godzinaPomiaruDataGridViewTextBoxColumn.DataPropertyName = "Godzina pomiaru";
            this.godzinaPomiaruDataGridViewTextBoxColumn.HeaderText = "Godzina pomiaru";
            this.godzinaPomiaruDataGridViewTextBoxColumn.Name = "godzinaPomiaruDataGridViewTextBoxColumn";
            this.godzinaPomiaruDataGridViewTextBoxColumn.Width = 125;
            // 
            // zamowieniaBindingSource
            // 
            this.zamowieniaBindingSource.DataMember = "Zamowienia";
            this.zamowieniaBindingSource.DataSource = this.databaseDataSet;
            // 
            // Pracownicy
            // 
            this.Pracownicy.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.Pracownicy.Controls.Add(this.dataGridView2);
            this.Pracownicy.Controls.Add(this.dataGridView1);
            this.Pracownicy.Controls.Add(this.label2);
            this.Pracownicy.Controls.Add(this.label1);
            this.Pracownicy.Location = new System.Drawing.Point(4, 22);
            this.Pracownicy.Name = "Pracownicy";
            this.Pracownicy.Padding = new System.Windows.Forms.Padding(3);
            this.Pracownicy.Size = new System.Drawing.Size(1222, 504);
            this.Pracownicy.TabIndex = 0;
            this.Pracownicy.Text = "Pracownicy i Produkty";
            this.Pracownicy.UseVisualStyleBackColor = true;
            this.Pracownicy.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.imieDataGridViewTextBoxColumn1,
            this.nazwiskoDataGridViewTextBoxColumn1});
            this.dataGridView2.DataSource = this.pomiarowcyViewBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(320, 25);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(302, 473);
            this.dataGridView2.TabIndex = 13;
            this.dataGridView2.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView2_CellMouseDoubleClick);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Width = 50;
            // 
            // imieDataGridViewTextBoxColumn1
            // 
            this.imieDataGridViewTextBoxColumn1.DataPropertyName = "Imie";
            this.imieDataGridViewTextBoxColumn1.HeaderText = "Imie";
            this.imieDataGridViewTextBoxColumn1.Name = "imieDataGridViewTextBoxColumn1";
            this.imieDataGridViewTextBoxColumn1.Width = 105;
            // 
            // nazwiskoDataGridViewTextBoxColumn1
            // 
            this.nazwiskoDataGridViewTextBoxColumn1.DataPropertyName = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn1.HeaderText = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn1.Name = "nazwiskoDataGridViewTextBoxColumn1";
            // 
            // pomiarowcyViewBindingSource
            // 
            this.pomiarowcyViewBindingSource.DataMember = "PomiarowcyView";
            this.pomiarowcyViewBindingSource.DataSource = this.databaseDataSet;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.imieDataGridViewTextBoxColumn,
            this.nazwiskoDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.monterzyViewBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(3, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(311, 473);
            this.dataGridView1.TabIndex = 12;
            this.dataGridView1.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDoubleClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Width = 50;
            // 
            // imieDataGridViewTextBoxColumn
            // 
            this.imieDataGridViewTextBoxColumn.DataPropertyName = "Imie";
            this.imieDataGridViewTextBoxColumn.HeaderText = "Imie";
            this.imieDataGridViewTextBoxColumn.Name = "imieDataGridViewTextBoxColumn";
            this.imieDataGridViewTextBoxColumn.Width = 110;
            // 
            // nazwiskoDataGridViewTextBoxColumn
            // 
            this.nazwiskoDataGridViewTextBoxColumn.DataPropertyName = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn.HeaderText = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn.Name = "nazwiskoDataGridViewTextBoxColumn";
            // 
            // monterzyViewBindingSource
            // 
            this.monterzyViewBindingSource.DataMember = "MonterzyView";
            this.monterzyViewBindingSource.DataSource = this.databaseDataSet;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(442, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Pomiarowcy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(139, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Monterzy";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Pracownicy);
            this.tabControl1.Controls.Add(this.Zamowienia);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(3, 88);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1230, 530);
            this.tabControl1.TabIndex = 5;
            this.tabControl1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseDoubleClick);
            // 
            // monterzyViewTableAdapter
            // 
            this.monterzyViewTableAdapter.ClearBeforeFill = true;
            // 
            // pomiarowcyViewTableAdapter
            // 
            this.pomiarowcyViewTableAdapter.ClearBeforeFill = true;
            // 
            // zamowieniaTableAdapter
            // 
            this.zamowieniaTableAdapter.ClearBeforeFill = true;
            // 
            // montażeTodayTableAdapter
            // 
            this.montażeTodayTableAdapter.ClearBeforeFill = true;
            // 
            // pomiaryTodayTableAdapter
            // 
            this.pomiaryTodayTableAdapter.ClearBeforeFill = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(845, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Zysk w tym miesiacu:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(845, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Przewidywany zysk na koniec:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(959, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "000";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1002, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "000";
            this.label8.Click += new System.EventHandler(this.label7_Click);
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1241, 621);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainUI";
            this.Text = "MainUI";
            this.Load += new System.EventHandler(this.MainUI_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MainUI_MouseUp);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pomiaryTodayBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.montażeTodayBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet)).EndInit();
            this.Zamowienia.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zamowieniaBindingSource)).EndInit();
            this.Pracownicy.ResumeLayout(false);
            this.Pracownicy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pomiarowcyViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monterzyViewBindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sAsdToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
 
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage Zamowienia;
        private System.Windows.Forms.TabPage Pracownicy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;


        private System.Windows.Forms.DataGridViewTextBoxColumn idklientDataGridViewTextBoxColumn;
        private DatabaseDataSet databaseDataSet;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource monterzyViewBindingSource;
        private DatabaseDataSetTableAdapters.MonterzyViewTableAdapter monterzyViewTableAdapter;
        private System.Windows.Forms.BindingSource pomiarowcyViewBindingSource;
        private DatabaseDataSetTableAdapters.PomiarowcyViewTableAdapter pomiarowcyViewTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.BindingSource zamowieniaBindingSource;
        private DatabaseDataSetTableAdapters.ZamowieniaTableAdapter zamowieniaTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwiskoDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ulicaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn miastoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numerTelefonuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wartośćDlaKlientaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataMontażuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn godzinaMontażuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataPomiaruDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn godzinaPomiaruDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.BindingSource montażeTodayBindingSource;
        private DatabaseDataSetTableAdapters.MontażeTodayTableAdapter montażeTodayTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn godzinaMontażuDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn imieDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwiskoDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ulicaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dzielnicaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn miastoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn numerTelefonuDataGridViewTextBoxColumn1;
        private DatabaseDataSet databaseDataSet1;
        private System.Windows.Forms.BindingSource pomiaryTodayBindingSource;
        private DatabaseDataSetTableAdapters.PomiaryTodayTableAdapter pomiaryTodayTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn godzinaPomiaruDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn imieDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwiskoDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ulicaDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dzielnicaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn miastoDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn numerTelefonuDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn imieDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwiskoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn imieDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwiskoDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}

