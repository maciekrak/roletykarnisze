﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel
{
    public class DataContext
    {
        //TODO: WRZUCIC WSZYSTKIE STAŁE / CONNECTION STRING ETC, I PRZEKAZAC W PARAMETR KAZDEJ KLASY, UNIKNIEMY TYM DUBLOWANIA ZMIENNYCH
        public static string connectionString = Properties.Settings.Default.DatabaseConnectionString;

        public string getConnectionString()
        {
            return connectionString;
        }
    }
}
