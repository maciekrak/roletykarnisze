﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminPanel.AppLogic;


using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

using System.Drawing;
using System.Drawing.Printing;

namespace AdminPanel.PdfCreator
{
    public class DataPrinter
    {
        private List<String> kartaKlienta { get; set; }
        private string[] pola = { "Numer zamówienia", "Nazwisko", "Ulica", "Miasto", "Numer telefonu", "Wartość",
        "Data montażu", "Godzina montażu", "Data pomiaru", "Godzina pomiaru", "Uwagi"};
        private string fileName { get; set; }

        public DataPrinter(List<string> _kk)
        {
            this.kartaKlienta = _kk;
            this.fileName = $@"{_kk[1]}_{_kk[0]}_{_kk[6].Replace('/','_').Replace('.','_')}.pdf";
        }

        public void dataToPdf()
        {
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(this.fileName, FileMode.Create));
            doc.Open();
            var listpol = pola.ToList();
            var lst = ConcatLists(listpol, kartaKlienta);
            //var list = pola.ToList().Concat(kartaKlienta).ToList();
            foreach (var pole in lst)
            {
                Paragraph par = new Paragraph(pole);
                doc.Add(par);
            }
            doc.Close();
        }
        public void OpenDocument()
        {
            System.Diagnostics.Process.Start($@"{fileName}");
        }

        public List<string> ConcatLists(List<string> A, List<string> b)
        {
            List<string> final = new List<string>();
            for(int i = 0; i < A.Count; i++)
            {               
               final.Add($"{A[i]}: {(b[i])}");
            }
            return final;
        }
    }
}
