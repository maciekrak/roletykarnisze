﻿using System;
using AdminPanel.AppLogic;
using System.Threading;
using System.Timers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using AdminPanel.PdfCreator;

namespace AdminPanel
{
    public partial class MainUI : Form
    {
        public static int clientId;
        public MainUI()
        {
            InitializeComponent();
            SetAllTextsValid();
        }
        
        private void aaaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void SetAllTextsValid()
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddWorker frm = new AddWorker(this);
            frm.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void MainUI_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'databaseDataSet1.PomiaryToday' table. You can move, or remove it, as needed.
            this.pomiaryTodayTableAdapter.Fill(this.databaseDataSet1.PomiaryToday);
            // TODO: This line of code loads data into the 'databaseDataSet.MontażeToday' table. You can move, or remove it, as needed.
            this.montażeTodayTableAdapter.Fill(this.databaseDataSet.MontażeToday);
            // TODO: This line of code loads data into the 'databaseDataSet.Zamowienia' table. You can move, or remove it, as needed.
            this.zamowieniaTableAdapter.Fill(this.databaseDataSet.Zamowienia);
            // TODO: This line of code loads data into the 'databaseDataSet.PomiarowcyView' table. You can move, or remove it, as needed.
            this.pomiarowcyViewTableAdapter.Fill(this.databaseDataSet.PomiarowcyView);
            // TODO: This line of code loads data into the 'databaseDataSet.MonterzyView' table. You can move, or remove it, as needed.
            this.monterzyViewTableAdapter.Fill(this.databaseDataSet.MonterzyView);
            // TODO: This line of code loads data into the 'databaseDataSet.PomiarowcyView' table. You can move, or remove it, as needed.
            this.pomiarowcyViewTableAdapter.Fill(this.databaseDataSet.PomiarowcyView);
            
            // TODO: This line of code loads data into the 'databaseDataSet.MonterzyView' table. You can move, or remove it, as needed.
            this.monterzyViewTableAdapter.Fill(this.databaseDataSet.MonterzyView);
            setFutureIncomeLabels();

        }
        public void setFutureIncomeLabels()
        {
            var cash = new AppLogic.Przychod.futureCash();
            label7.Text = cash.allCash.ToString();
            label8.Text = cash.approxCas.ToString();
        }
        public void listbox2Refresh()
        {
            
        }

        private void listBox2_DataSourceChanged(object sender, EventArgs e)
        {
                        
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void MainUI_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

       /// private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
     //   {
      //      // MessageBox.Show(listBox2.GetItemText(listBox2.SelectedItem));
      //      //MessageBox.Show(listBox2.GetItemText(listBox2.SelectedItem));
     // //     var contentList =  listBox2.GetItemText(listBox2.SelectedItem).Split(' ').ToList().Where(x=>!x.Equals("")).ToList();
     //      var frm = new WorkerView(contentList[0], contentList[1], "Monter", this);
    //       frm.Show();
     //   }

        private void button2_Click(object sender, EventArgs e)
        {
            AddClientCardForm frm = new AddClientCardForm();
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddProductCard frm = new AddProductCard();
            frm.Show();
        }

        private void monterzyBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void fillIdToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void monterzyBindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var CalendarView = new CalendarView();
            CalendarView.Show();
        }

        private void listBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            List<string> loc = new List<string>();
            if (e.RowIndex >= 0) {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                for(int i =0;i<=2;i++)
                {
                    loc.Add(row.Cells[i].Value.ToString().Replace(" ", string.Empty));
                }
                    }
            var frm = new WorkerView(loc[1], loc[2], "Monter", this);
            frm.Show();
        }

        private void dataGridView2_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            List<string> loc = new List<string>();
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView2.Rows[e.RowIndex];
                for (int i = 0; i <= 2; i++)
                {
                    loc.Add(row.Cells[i].Value.ToString().Replace(" ", string.Empty));
                }
            }
            var frm = new WorkerView(loc[1], loc[2], "Pomiarowiec", this);
            frm.Show();
        }



        public void ticker()
        {
            dataGridView1.DataSource = databaseDataSet.MonterzyView;
            dataGridView2.DataSource = databaseDataSet.PomiarowcyView;

            this.pomiaryTodayTableAdapter.Fill(this.databaseDataSet1.PomiaryToday);
            // TODO: This line of code loads data into the 'databaseDataSet.MontażeToday' table. You can move, or remove it, as needed.
            this.montażeTodayTableAdapter.Fill(this.databaseDataSet.MontażeToday);
            // TODO: This line of code loads data into the 'databaseDataSet.Zamowienia' table. You can move, or remove it, as needed.
            this.zamowieniaTableAdapter.Fill(this.databaseDataSet.Zamowienia);
            // TODO: This line of code loads data into the 'databaseDataSet.PomiarowcyView' table. You can move, or remove it, as needed.
            this.pomiarowcyViewTableAdapter.Fill(this.databaseDataSet.PomiarowcyView);
            // TODO: This line of code loads data into the 'databaseDataSet.MonterzyView' table. You can move, or remove it, as needed.
            this.monterzyViewTableAdapter.Fill(this.databaseDataSet.MonterzyView);
            // TODO: This line of code loads data into the 'databaseDataSet.PomiarowcyView' table. You can move, or remove it, as needed.
            this.pomiarowcyViewTableAdapter.Fill(this.databaseDataSet.PomiarowcyView);

            // TODO: This line of code loads data into the 'databaseDataSet.MonterzyView' table. You can move, or remove it, as needed.
            this.monterzyViewTableAdapter.Fill(this.databaseDataSet.MonterzyView);
            dataGridView1.Refresh();
            dataGridView2.Refresh();
        }

        private void dataGridView4_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            List<string> loc = new List<string>();
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView4.Rows[e.RowIndex];
                for (int i = 0; i <= 9; i++)
                {
                    loc.Add(row.Cells[i].Value.ToString());//.Replace("12:00:00", string.Empty).Replace(" ", string.Empty));
                }
            }
            var frm = new OrderView(loc);
            frm.Show();
        }

        private void dataGridView4_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            List<string> loc = new List<string>();
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView4.Rows[e.RowIndex];
                for (int i = 0; i <= 9; i++)
                {
                    loc.Add(row.Cells[i].Value.ToString());//.Replace("12:00:00", string.Empty).Replace(" ", string.Empty));
                }
            }
            var frm = new OrderView(loc);
            frm.Show();
        }

        private void dataGridView5_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        // private void tabControl1_MouseEnter(object sender, EventArgs e)
        //  {
        //      ticker();
        //   }

        // private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        //  {
        //     var regex = new Regex("  +");
        // var contentList = regex.Split(listBox1.GetItemText(listBox1.SelectedItem)).ToList();
        // TaskView frm = new TaskView("Pomiar: ", contentList);
        //  var contentList = listBox.GetItemText(listBox5.SelectedItem).Split(' ').ToList().Where(x => !x.Equals("")).ToList();


        //frm.listofInfo = contentList;
        //PdfHandler frm = new PdfHandler();
        // frm.Show();
        // }

        //  private void listBox6_MouseDoubleClick(object sender, MouseEventArgs e)
        //  {
        //      var regex = new Regex("  +");
        //var contentList = regex.Split(listBox5.GetItemText(listBox5.SelectedItem)).ToList();
        //  TaskView frm = new TaskView("Montaż: ", contentList);

        // var contentList = listBox5.GetItemText(listBox5.SelectedItem).Split(' ').ToList().Where(x => !x.Equals("")).ToList();
        //frm.listofInfo = contentList;
        // PdfHandler frm = new PdfHandler();
        //     frm.Show();
        //  }
    }
}
