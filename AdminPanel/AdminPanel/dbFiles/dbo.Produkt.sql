﻿CREATE TABLE [dbo].[Produkt] (
    [Id_produktu] INT        IDENTITY (1, 1) NOT NULL,
    [Cena]        SMALLMONEY NULL,
    [Id_klient]   INT        NULL,
    [Nazwa]       NCHAR (10) NULL,
    [Status]      NCHAR (10) NULL,
    [Ilość]       NCHAR (10) NULL,
    [Wymiar]      NCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([Id_produktu] ASC), 
    CONSTRAINT KlientFK FOREIGN KEY (Id_klient) REFERENCES Klient(Id_klient)
);

