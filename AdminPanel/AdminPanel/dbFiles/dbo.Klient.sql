﻿CREATE TABLE [dbo].Klient
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Imię] NCHAR(10) NULL, 
    [Nazwisko] NCHAR(10) NULL, 
    [Ulica] NCHAR(10) NULL, 
    [Dzielnica] NCHAR(10) NULL, 
    [Miasto] NCHAR(10) NULL, 
    [Kod pocztowy] NCHAR(10) NULL, 
    [Numer telefonu] NCHAR(10) NULL, 
    [Uwagi] NCHAR(10) NULL, 
    [Data pomiaru] DATETIME NULL, 
    [Czas pomiaru] SMALLINT NULL, 
    [Os. odpowiedzialna za pomiar] NCHAR(10) NULL, 
    [Data montażu] DATETIME NULL, 
    [Czas montażu] SMALLINT NULL, 
    [Monter] NCHAR(10) NULL, 
    [Zamówione produkty] VARCHAR(50) NULL, 
    [Zaliczka] VARCHAR(50) NULL, 
    [Wartość cennikowa] SMALLMONEY NULL, 
    [Wartość dla klienta] SMALLMONEY NULL, 
    [Rozliczenie] VARCHAR(50) NULL
)
