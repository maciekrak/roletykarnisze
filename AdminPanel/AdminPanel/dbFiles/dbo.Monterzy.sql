﻿CREATE TABLE [dbo].[Monterzy] (
    [Id_monter] INT        IDENTITY (1, 1) NOT NULL,
    [Imie]      NCHAR (10) NULL,
    [Nazwisko]  NCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([Id_monter] ASC)
);

